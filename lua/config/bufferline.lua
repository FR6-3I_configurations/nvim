local diagnostics_indicator = function(count, level, diagnostics_dict, context)
  local s = ' '
  for e, n in pairs(diagnostics_dict) do
    local sym = e == 'error' and ' '
      or (e == 'warning' and ' ' or '' )
    s = s .. n .. sym
  end
  return s
end

require('bufferline').setup {
  options = {
    separator_style = 'thick',
    show_close_icon = false,

    close_command = 'BufDel %d',
    middle_mouse_command = 'BufDel %d',
    right_mouse_command = '',

    diagnostics = 'nvim_lsp',
    diagnostics_indicator = diagnostics_indicator,

    offsets = {
      {
        filetype = 'NvimTree',
        padding = 1,
        text = 'Répertoire'
      }
    }
  }
}

