require('nvim-tree').setup {
  hijack_cursor = true,
  open_on_setup = true,

  diagnostics = {
    enable = true,
    show_on_dirs = true
  },

  git = {
    ignore = false
  },

  renderer = {
    indent_markers = {
      enable = true
    }
  },

  update_focused_file = {
    enable = true
  }
}
