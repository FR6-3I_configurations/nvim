local cmp = require('cmp')
local lspkind = require('lspkind')
local MAX_ITEM = 20

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

local servers = {
  'bashls',
  'cssls',
  'eslint',
  'graphql',
  'html',
  'jsonls',
  'prismals',
  'rust_analyzer',
  'sumneko_lua',
  'tsserver',
  'yamlls'
}

for _, lsp in ipairs(servers) do
  require('lspconfig')[lsp].setup {
    capabilities = capabilities,
  }
end

-- nvim-cmp
cmp.setup {
  completion = {
    completeopt = 'menu, menuone, noinsert'
  },

  sources = {
    { name = 'nvim_lsp', max_item_count = MAX_ITEM },
    { name = 'rg', max_item_count = MAX_ITEM },
    { name = 'zsh', max_item_count = MAX_ITEM },
    { name = 'crates', max_item_count = MAX_ITEM }
  },

  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end
  },

  formatting = {
    format = lspkind.cmp_format({
      mode = 'symbol_text',
      maxwidth = 50,

      menu = ({
        nvim_lsp = '[LSP]',
        rg = '[RG]'
      })
    })
  }
}

-- cmp-cmdline
cmp.setup.cmdline(':', {
  sources = {{
    name = 'cmdline',
    max_item_count = MAX_ITEM
  }}
})

-- cmp-buffer
cmp.setup.cmdline('/', {
  sources = {{
    name = 'buffer',
    max_item_count = MAX_ITEM
  }}
})

