require('lualine').setup {
  options = {
    section_separators = '',
    component_separators = ''
  },

  sections = {
    lualine_c = {'filename', 'filesize'}
  },

  inactive_sections = {
    lualine_c = {'filename', 'filesize'}
  },

  extensions = {
    'nvim-tree'
  }
}
