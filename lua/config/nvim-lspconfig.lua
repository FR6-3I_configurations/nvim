require('nvim-lsp-installer').setup {
  automatic_installation = true
}

local lspconfig = require('lspconfig')

local signs = {
  Error = '',
  Warn = '',
  Hint = '',
  Info = ''
}

for type, icon in pairs(signs) do
  local hl = 'DiagnosticSign' .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl })
end

-- Bash
lspconfig.bashls.setup {}

-- CSS
lspconfig.cssls.setup {}

-- Eslint
lspconfig.eslint.setup {
  handlers = {
    ['window/showMessageRequest'] = function(_, result) return result end
  }
}

-- GraphQL
lspconfig.graphql.setup {}

-- HTML
lspconfig.html.setup {}

-- JSON
lspconfig.jsonls.setup {}

-- Prisma
lspconfig.prismals.setup {}

-- Rust
lspconfig.rust_analyzer.setup {}

-- Lua
lspconfig.sumneko_lua.setup {
  settings = {
    Lua = {
      diagnostics = {
        globals = { 'execute', 'vim' }
      },
      telemetry = {
        enable = false
      }
    }
  }
}

-- TypeScript
lspconfig.tsserver.setup {}

-- YAML
lspconfig.yamlls.setup {}
