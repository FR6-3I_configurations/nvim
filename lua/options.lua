local opt = vim.opt

-- Leader key
vim.g.mapleader = 'ù'

-- Mouse
opt.mouse = 'a'

-- Line number
opt.number = true
opt.relativenumber = true

-- Tabulation
opt.tabstop = 2
opt.shiftwidth = 2
opt.expandtab = true
