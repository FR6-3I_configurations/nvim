require('nvim-treesitter.configs').setup {
  ensure_installed = {
    'bash',
    'css',
    'graphql',
    'html',
    'javascript',
    'json',
    'lua',
    'markdown',
    'prisma',
    'rust',
    'toml',
    'tsx',
    'typescript',
    'yaml'
  }
}
