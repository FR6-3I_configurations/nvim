local keymap = vim.keymap.set
local cmp = require('cmp')

-- Nvim-lspconfig
keymap('n', '(d', vim.diagnostic.goto_prev, opts)
keymap('n', ')d', vim.diagnostic.goto_next, opts)
keymap('n', '<C-o>', vim.diagnostic.open_float)
keymap('n', 'gd', vim.lsp.buf.definition)
keymap('n', 'K', vim.lsp.buf.hover)
keymap('n', 'gi', vim.lsp.buf.implementation)
keymap('n', 'rn', vim.lsp.buf.rename)
keymap('n', 'gr', vim.lsp.buf.references)

-- Nvim-cmp
cmp.setup {
  mapping = {
    ['<Down>'] = cmp.mapping(
      cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
      { 'i' }
    ),
    ['<Up>'] = cmp.mapping(
      cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
      { 'i' }
    ),
    ['<C-Down>'] = cmp.mapping(
      cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
      { 'c' }
    ),
    ['<C-Up>'] = cmp.mapping(
      cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
      { 'c' }
    ),
    ['<C-Space>'] = cmp.mapping(
      function()
        if cmp.visible() then cmp.close()
        else cmp.complete()
        end
      end
    , { 'i', 'c' }),
    ['<CR>'] = cmp.mapping.confirm({ select = true })
  }
}

--  Gitsigns
keymap('n', '<M-g>d', ':Gitsigns preview_hunk<CR>')
keymap('n', '<M-g>p', ':Gitsigns prev_hunk<CR>')
keymap('n', '<M-g>n', ':Gitsigns next_hunk<CR>')

-- Save actual buffer
keymap('n', '<C-s>', ':w<CR>')

-- Quit NeoVim (without saving)
keymap('n', '<C-Del>', ':qa<CR>')
