local fn = vim.fn
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'

function Get_config(name)
  return string.format("require('config.%s')", name)
end

-- Install packer if it's not installed
if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({
    'git', 'clone', 'https://github.com/wbthomason/packer.nvim',
    install_path
  })

  execute 'packadd packer.nvim'
end

local packer = require('packer')
local use = packer.use

packer.init()
packer.reset()

use 'wbthomason/packer.nvim'

-- Buffer list
use {
  'akinsho/bufferline.nvim',
  requires = 'kyazdani42/nvim-web-devicons',
  config = Get_config('bufferline')
}

-- Match words
use 'andymass/vim-matchup'

-- Highlight CSS colors
use 'ap/vim-css-color'

-- Theme
use {
  'ayu-theme/ayu-vim',
  config = Get_config('ayu-vim')
}

-- Automatic JS imports
use  'Galooshi/vim-import-js'

-- Auto-complete
use {
  'hrsh7th/nvim-cmp',
  requires = {
    'hrsh7th/cmp-nvim-lsp',
    'L3MON4D3/LuaSnip',
    'saadparwaiz1/cmp_luasnip',
    'hrsh7th/cmp-cmdline',
    'hrsh7th/cmp-buffer',
    'lukas-reineke/cmp-rg'
  },
  config = Get_config('nvim-cmp')
}

-- File explorer
use {
  'kyazdani42/nvim-tree.lua',
  requires = 'kyazdani42/nvim-web-devicons',
  config = Get_config('nvim-tree')
}

-- Git informations
use {
  'lewis6991/gitsigns.nvim',
  config = Get_config('gitsigns')
}

-- Indentation
use {
  'lukas-reineke/indent-blankline.nvim',
  config = Get_config('indent-blankline')
}

-- Vertical column indicator
use {
  'lukas-reineke/virt-column.nvim',
  config = Get_config('virt-column')
}

-- LSP
use {
  'neovim/nvim-lspconfig',
  requires = 'williamboman/nvim-lsp-installer',
  config = Get_config('nvim-lspconfig')
}

-- Comments
use {
  'numToStr/Comment.nvim',
  config = Get_config('comment')
}

-- Bottom NeoVim line
use {
  'nvim-lualine/lualine.nvim',
  requires = {
    'kyazdani42/nvim-web-devicons',
    opt = true
  },
  config = Get_config('lualine')
}

-- Tree sitter
use {
  'nvim-treesitter/nvim-treesitter',
  run = ':TSUpdate'
}

-- Focus new buffer when closing an other buffer
use {
  'ojroques/nvim-bufdel',
  config = Get_config('nvim-bufdel')
}

-- Auto-complete pictograms
use 'onsails/lspkind.nvim'


-- LSP signature informations
use {
  'ray-x/lsp_signature.nvim',
  config = Get_config('lsp_signature')
}

-- Save sessions automatically
use 'rmagatti/auto-session'

-- Cargo crates
use {
  'saecki/crates.nvim',
  event = 'BufRead Cargo.toml',
  requires = 'nvim-lua/plenary.nvim',
  config = Get_config('crates')
}

-- Transparent background
use 'tribela/vim-transparent'

-- Cursor style
use {
  'yamatsum/nvim-cursorline',
  config = Get_config('nvim-cursorline')
}
