# ⚠️ This repository is deprecated in favor of [this repository](https://gitlab.com/FR6-3I_configurations/helix).

<div align='center'>

# NeoVim

</div>

## 📦 Dependencies (Arch)

### ❗ Required

Official :

```shell
paru -S neovim nodejs ripgrep
```

NPM :

```shell
pnpm i -g import-js
```

### ➕ Optional

Official :

```shell
paru -S \
  wl-clipboard \ # Wayland clipboard
  xclip # X11 clipboard
```

## ⚙️ Configuration

Install extensions (in NeoVim) :

```
:PackerSync
```

Close and reopen NeoVim, wait that all LSP are installed.
Command to see it :

```
:LspInstallInfo
```

Just close NeoVim again. It's now usable!
