-- Update config for each modification in plugins.lua
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup END
]])

-- Delete all blank spaces in end of line
vim.cmd([[
  augroup TrimWhiteSpace
    autocmd!
    autocmd BufWritePre * :%s/\s\+$//e
  augroup END
]])

-- Fix Eslint some errors when saving file
vim.cmd([[
  augroup eslint
    autocmd!
    autocmd BufWritePre *.tsx,*.ts,*.jsx,*.js EslintFixAll
  augroup END
]])
